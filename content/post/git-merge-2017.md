+++
date = "2017-02-05T16:37:21+01:00"
title = "Git Merge 2017"
draft = false

+++

This year I've finally managed to go to the
[FOSDEM](https://fosdem.org/2017/), but just before that there was the [Git
Merge](http://git-merge.com/) in [The Egg](http://www.theeggbrussels.com/).
It was a really nice, well-organized event with a lot of friendly IT people
around. And I've learnt a lot :)!

At first I've heard a bit about [Software Freedom
Conservancy](https://sfconservancy.org/) and
[Outreachy](https://www.gnome.org/outreachy/) (it's something like
[GSoC](https://developers.google.com/open-source/gsoc/, but aiming to
diversify FLOSS community), which I hadn't known before. I like the fact, that
so many people can get some support while working on Free and Open Source
Software.

Next, as you can notice on the schedule, there were several talks about the
scale of the Git/Mercurial repositories. What surprised me the most was the fact,
that there are _serious_ repositories (Windows), for which `git checkout` and
other commands take ages to run. Because of that, Microsoft has created
[GVFS](https://github.com/Microsoft/gvfs) shortening checkout time
**from 3 hours to 30 seconds**. At the other hand, Durham Goode gave a talk
about the way software version control is organized in the Facebook (reasons
for choosing Mercurial can be found
[here](https://code.facebook.com/posts/218678814984400/scaling-mercurial-at-facebook/).
By the way, before the talks, during a breakfast, I've also learnt about
[Mercurial's evolve extension](http://www.gerg.ca/evolve/user-guide.html),
which adjusts and synchronizes history between repositories after changing it on
one host (e.g. amending HEAD~8). Definitely I should take a closer look at the
Mercurial (it's written in Python).

Git complexity has been mentioned many times, together with a lot of
suggestions how we could make it easier to work with it, teach it more
effectively and build helpful abstractions over it. Santiago Perez De Rosso
told us "What’s Wrong With Git?" and presented [Gitless](http://gitless.com/),
which is roughly a "version control system built on top of Git" with simplified
interface. Beside that, Tim Pettersen talked about Git aliases and the main
thing I remember is that
[wiki page with aliases is quite useful](https://www.mediawiki.org/wiki/Git/aliases)
;).

I've also had a good time during afterparty where, among other things, I've
talked to one of JetBrains' developers about version control systems and how
IDEs help developers coping with them. I belive that in the future IDEs may
be excellent tools for creating abstractions over the things like virtual file
systems and complicated interfaces/additional tools.

During the last talk, "Trust But Verify" by Thordur Bjornsson, I've heard a
really important tip about setting an expiration date of my PGP keys (to one
year at most). Unfortunately I've never thought about that :<.

---

At the end I'd like to recommend you a [blog](http://jvns.ca/) written by
[Julia](https://twitter.com/b0rk) [Evans](https://github.com/jvns). Thanks to
her I realized that a post about things you don't know can be as interesting as
a detailed explaination of some "arcane" technical knowledge. She's even
written [that great advice for new
bloggers](http://jvns.ca/blog/2016/05/22/how-do-you-write-blog-posts/), which
was really helpful. Also, take a look at [her zines](http://jvns.ca/zines/) and
[technical drawings](https://drawings.jvns.ca/).
