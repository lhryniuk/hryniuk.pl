+++
date = "2015-12-28T00:00:00+01:00"
title = "rofi_notes"
draft = false

+++

Some time ago I was thinking about (and looking for) a program for taking
notes. Simple, small, one that will be “around”, but won’t disturb, covering
part of screen. I’ve been using `rofi` for a while and I wanted to have
something similar. After a quick look at Useful scripts page I realized that
with `–dmenu` option (which allows you to use your input to show in the rofi
window) solution is obvious – I can keep all in a single file and modify it
with rofi. So I wrote this script I called `rofi_notes` to push and pull notes:

<div align="center">
<img src="/images/rofi-notes.png">
</div>

Text format allows me to use the whole zoo of command line tools to manage them
automatically and connect it with other things I use every day, e.g.
`i3pystatus`, which you can see below – the number on the right of the
exclamation indicates how many notes I currently have (on the left you can
notice workspaces, on the right from notes info: weather, uptime, free space,
used memory and load).

<div align="center">
<img src="/images/rofi-notes-on-i3pystatus.png">
</div>

As I mention, you can moreover use tools like sort and cut to e.g. put
priorities on your list – just modify my script to put a priority number at the
first column and hide numbers before passing them to rofi. Or display only five
first items. Or show the first note on the status bar. Or… you can change this
little script to send inserted note on your e-mail address or… put it into the
calendar, if you passed date. With rofi the sky it the limit ;-).
