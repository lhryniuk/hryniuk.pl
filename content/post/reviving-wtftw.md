+++
date = "2018-04-18T19:34:07+02:00"
title = "Reviving WTFTW"

+++

## TL;DR

To run WTFTW:

* get version with my fixes (described below): https://github.com/hryniuk/wtftw
* make sure you've got `xmessage` installed to see error messages
* take a look on these two issues:
  ["gnome-terminal doesn't draw at all"](https://github.com/Kintaro/wtftw/issues/98) and
  ["Java GUI appears blank"](https://github.com/Kintaro/wtftw/issues/79), you'll
  most likely encounter both.
* apply
  [this commit](https://github.com/hryniuk/wtftw/commit/ae6217de83554e9b4e1555f54fdf4006d6eb3ed2)
  to enable logging to terminal (great for testing)

---

## Intro

I've started playing with Rust and after a few weeks, with a good grasp of
basics, I've been looking for a bigger project to dig around and learn.  Beside
popular projects like [Alacritty](https://github.com/jwilm/alacritty) and
[Xi](https://github.com/google/xi-editor) I've found
[wtftw](https://github.com/Kintaro/wtftw) - a **tiling window manager** written
completely in **Rust**!.

It seemed to ~~be stable~~ somehow work, so I've decided to switch from my old
i3 configuration to it. At the time, the latest change was commited on Sep 19,
2017 and the README mentioned compilation against Rust 1.2.0+ (1.2.0 has been
released on Aug 7, 2015, long long time ago), so I had a few problems with
building it with 3 years older compiler/dependencies, hence this post.

## Compiling master branch

The first problem were outdated dependencies, which was trivial to fix with `cargo`:

```shell
$ cargo update
```

then, after running `cargo build` I was able to run it under
[Xephyr](https://en.wikipedia.org/wiki/Xephyr) (see also WTFTW's README) and,
finally, outside it:

<div align="center">
<img style="height: 300px" src="/images/wtftw-on-xephyr.png">
</div>

## Custom config

So next, I've tried to add more shortcuts to make it more convenient.

**README**: "take a look at the example config in `config/`. After the first
start, the config needs to be placed in `~/.wtftw/src/config.rs`.  Voila.".

It didn't work :<. It
seemed that WTFTW haven't even started... I've checked the code and realized
that I should get a proper error message, but... I didn't have `xmessage`
installed. After installing it (`xorg-xmessage` package in Arch Linux), I got
the actual error:

```
error compiling config module. run 'cargo build' in ~/.wtftw to get more info.
```

so, run `cargo build` and then:

```shell
error[E0554]: #![feature] may not be used on the stable release channel
 --> src/config.rs:1:1
  |
1 | #![feature(alloc_jemalloc)]
  | ^^^
```

When you search for it, you'll most probably reach [that page](https://doc.rust-lang.org/beta/unstable-book/library-features/alloc-jemalloc.html)
saying **"This feature has been replaced by the jemallocator crate on
crates.io."** I've tried to use it, but met another problems with compilation
and I've removed it completely. I mean these lines:

```rust
#![feature(alloc_jemalloc)]
extern crate alloc_jemalloc;
```

And nothing happened... which turned out to be a bit tricky coincidence:

* `rustc` (I belive) beside `libconfig.so` generates also `libconfig.d` file
* WTFTW
  [had](https://github.com/Kintaro/wtftw/blob/13712d4c051938520b90b6639d4ff813f6fe5f48/core/src/config.rs#L253)
  hard-coded only a prefix of the library's name (which is also described in
  [this issue](https://github.com/Kintaro/wtftw/issues/108)):

```rust
&Ok(ref y) => y.path().into_os_string().as_os_str().to_str()
               .unwrap().contains("libconfig"),
```

* library opening errors
  [were ignored](https://github.com/Kintaro/wtftw/blob/13712d4c051938520b90b6639d4ff813f6fe5f48/core/src/config.rs#L258)
  by using
  [`if let` syntax](https://doc.rust-lang.org/book/second-edition/ch06-03-if-let.html)

So, another easy fix, I've changed that name in the code to include `.so`
extension and... that was all. **I've reached a point, where I was able to run
it instead of i3 :)**.

## Logs?

I was looking for a log file to see what's going on, but with no success. If
you search for it in the source code, logs should be printed to
`$HOME/.wtftw.log`:
```shell
core/src/config.rs: logfile: format!("{}/.wtftw.log", home),
```
I don't know why, but it didn't worked for me. Soo...  I've added terminal
logger, which you can find in
[this commit](https://github.com/hryniuk/wtftw/commit/ae6217de83554e9b4e1555f54fdf4006d6eb3ed2).


## Problems left

I'm running WTFTW quite comfortable now, but there are a few issues that you
should be aware of:

* some applications, like PyCharm doesn't appear - only a gray rectangle shows
  up ([issue link](https://github.com/Kintaro/wtftw/issues/79); `export _JAVA_AWT_WM_NONREPARENTING=1`
  doesn't work all the time for me; haven't tried `wmname`):

<div align="center">
<img style="height: 300px" src="/images/gray-pycharm-bug.png">
</div>

* WTFTW sometimes panics and the error is only displayed "underneath", to the
  console; eg. when you misspell command of your launcher:

```shell
thread '<unnamed>' panicked at 'unable to start launcher', /home/hryniuk/.cargo/git/checkouts/wtftw-339623a2afc88a74/13712d4/core/src/handlers.rs:71:26
note: Some details are omitted, run with `RUST_BACKTRACE=full` for a verbose backtrace.
stack backtrace:
   0: std::sys::unix::backtrace::tracing::imp::unwind_backtrace
   1: std::sys_common::backtrace::_print
   2: std::panicking::default_hook::{{closure}}
   3: std::panicking::default_hook
   4: std::panicking::rust_panic_with_hook
   5: std::panicking::begin_panic
             at /build/rust/src/rustc-1.25.0-src/src/libstd/panicking.rs:537
   6: wtftw::handlers::default::start_launcher::{{closure}}
             at /home/hryniuk/.cargo/git/checkouts/wtftw-339623a2afc88a74/13712d4/core/src/handlers.rs:71
```

That's a drawback of compiling a config during WM start, but, of course, such a
problem can be handled in a more user-friendly way instead of panicking.

---

You can get version with all fixes I've described here: https://github.com/hryniuk/wtftw


